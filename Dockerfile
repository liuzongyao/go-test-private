FROM harbor-int.alauda.cn/e2e-automation/helloworld:latest
LABEL Version="1.1.90790791119111"
ENV VERSION=goq
COPY a.sh /
CMD echo $VERSION && sleep 60
